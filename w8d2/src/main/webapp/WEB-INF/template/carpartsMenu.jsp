<%@ include file="/WEB-INF/layouts/include.jsp" %>
<nav class="navbar bg-light">
	<!-- Links -->
	<!-- <ul class="navbar-nav"> -->
	<ul class="nav nav-pills flex-column">
		<li class="nav-item">
			<a class="${active eq 'H4666ST' ? 'active nav-link' : 'nav-link'}"
				href="<c:url value='/carparts/electrical/H466ST' />">
				Headlight Bulb
			</a>	
		</li>
		<li class="nav-item">
			<a class="${active eq 'M134HV' ? 'active nav-link' : 'nav-link'}"
				href="<c:url value='/carparts/engine/M134HV' />">
					Oil pumps
				</a>				
		</li>
		<li class="nav-item">
			<a class="${active eq '40026' ? 'active nav-link' : 'nav-link'}"
				href="<c:url value='/carparts/other/40026' />">
					Air pump
			</a>
		</li>
	</ul>
</nav>