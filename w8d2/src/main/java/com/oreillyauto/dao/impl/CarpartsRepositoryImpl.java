package com.oreillyauto.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import com.oreillyauto.dao.custom.CarpartsRepositoryCustom;
import com.oreillyauto.domain.Carpart;
import com.oreillyauto.domain.QCarpart;

@Repository
public class CarpartsRepositoryImpl extends QuerydslRepositorySupport implements CarpartsRepositoryCustom {

	QCarpart carpartTable = QCarpart.carpart;
	
	public CarpartsRepositoryImpl() {
		super(Carpart.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Carpart> getCarparts() {
		String sql = "SELECT * " + 
	                 "  FROM carparts ";
		Query query = getEntityManager().createNativeQuery(sql);
		return (List<Carpart>) query.getResultList();
	}
	
	@Override
	public Carpart getCarpartByPartNumber(String partNumber) {
		
		return new Carpart(partNumber, "", "", "");
		
/*		//Carpart carpart = new Carpart();

		switch (partNumber) {
		
		// OIL PUMP
		// https://www.oreillyauto.com/detail/b/melling-4252/engine-parts---mounts-16774/oil-pump---components-16588/oil-pump-12147/82f2bc031cf9/melling-oil-pump/m134hv/4439339
		case "M134HV":
			carpart.setTitle("Melling Oil Pump");
			carpart.setPartNumber("M134HV");
			carpart.setLine("MEL");
			carpart.setDescription("Oil Pump  High Volume Pump; Intermediate Drive Shaft Included");
			//carpart.setStoreLocations(Arrays.asList(new Integer[] { 1243, 987, 676 }));
			carpart.setDetailsMap(getOilPumpDetails());
			break;
			
		// HEADLIGHT BULB
		// https://www.oreillyauto.com/detail/b/silverstar-5024/lighting---electrical-16777/headlights---fog-lights-25139/headlight-bulb-11519/1395fe3e34c6/sylvania-h4666-silverstar-halogen-sealed-beam-bulb-pack-of/h4666st/4743294
		case "H4666ST": 
			carpart.setTitle("Sylvania SilverStar Coated H4 Sealed Beam Headlight");
			carpart.setPartNumber("H4666ST");
			carpart.setLine("SYL");
			carpart.setDescription("Headlight Bulb  High Beam And Low Beam; Boxed");
			//carpart.setStoreLocations(Arrays.asList(new Integer[] { 1243, 987, 676 }));
			carpart.setDetailsMap(new HashMap<String, String>());
			break;
			
		// AIR PUMP
		// https://www.oreillyauto.com/detail/b/slime-4935/agriculture-hd-parts---accessories-19818/agricultural---construction-20126/tire-repair-20137/tire-inflators---sealants-25157/tire-inflators---air-pumps-17480/943407206ee1/slime-tire-inflator/40026/4721563
		case "40026":
			carpart.setTitle("Slime Tire Inflator");
			carpart.setPartNumber("40026");
			carpart.setLine("SLI");
			carpart.setDescription("");
			//carpart.setStoreLocations(Arrays.asList(new Integer[] { 1243, 676 }));
			carpart.setDetailsMap(getAirPumpDetails());
			break;
		default: // NOT FOUND
			carpart.setTitle("Part Number Not Found");
			carpart.setPartNumber("");
			carpart.setLine("");
			carpart.setDescription("");
			//carpart.setStoreLocations(new ArrayList<Integer>());
			carpart.setDetailsMap(new HashMap<String, String>());
			break;
		}

		return carpart;*/
	}

	private Map<String, String> getAirPumpDetails() {
		Map<String, String> detailsMap = new HashMap<String, String>();
		detailsMap.put("Maximum Pressure (psi)", "150 psi");
		detailsMap.put("Voltage (V)", "120 Volt");
		return detailsMap;
	}

	private Map<String, String> getOilPumpDetails() {
		Map<String, String> detailsMap = new HashMap<String, String>();
		detailsMap.put("New Or Remanufactured", "New");
		detailsMap.put("Gasket Or Seal", "No");
		detailsMap.put("Pickup Screen Included", "Yes");
		return detailsMap;
	}

	@Override
	public Carpart getCarpart(String partNumber) {
		Carpart carpart = (Carpart) getQuerydsl().createQuery()
                .from(carpartTable)
                .where(carpartTable.partNumber.eq(partNumber))
                .fetchOne();
        return carpart;
	}

}
