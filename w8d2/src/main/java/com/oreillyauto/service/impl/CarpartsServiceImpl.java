package com.oreillyauto.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oreillyauto.dao.CarpartsRepository;
import com.oreillyauto.domain.Carpart;
import com.oreillyauto.service.CarpartsService;

@Service
public class CarpartsServiceImpl implements CarpartsService {
    @Autowired
    CarpartsRepository carpartsRepo;

    @Override
    public Carpart getCarpartByPartNumber(String partNumber) {
        return carpartsRepo.findByPartNumber(partNumber);
    }
}
